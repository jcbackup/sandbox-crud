# Laravel 5.2 Sandbox CRUD Generator

**THIS PACKAGE IS HEAVILY COUPLED TO SOME PRIVATE DEPENDENCIES.**

If you are looking for a generic package, check out the great work made by Sohel Amin in his [CRUD generator](https://github.com/appzcoder/crud-generator).

**Quick reference:**

```
php artisan make:crud post --schema=title:string,body:text,published:boolean,published_at:timestamp
```
