<?php

namespace JoelCuevas\SandboxCrud\Commands;

use Illuminate\Console\Command;

use File;

class CrudViewCommand extends Command
{
    protected $signature = 'make:crud:view
        {name : The name of the resource.}
        {--schema= : The schema of the resource.}';

    protected $description = 'Create the resource views';

    public function handle()
    {
        $crud = str_singular(strtolower($this->argument('name')));
        $crud_cap = ucwords($crud);
        $crud_plural = str_plural($crud);
        $crud_plural_cap = ucwords($crud_plural);

        $schema = explode(',', $this->option('schema'));

        $fields = [];

        foreach ($schema as $field) {
            $parts = explode(':', $field);

            $label = preg_replace('/(.*)_id$/', '$1', trim($parts[0]));

            $fields[] = [
                'name' => trim($parts[0]),
                'type' => trim($parts[1]),
                'label' => ucfirst(strtolower(str_replace('_', ' ', $label))),
            ];
        }

        $form_fields = [];

        foreach ($fields as $field) {
            $field_type = $this->getFieldControl($field['type']);
            $form_fields[] = $this->getControlStub($field_type, $field['name'], $field['label']);
        }

        $form_fields = implode(PHP_EOL . PHP_EOL, $form_fields);

        $headers = [];
        $show_thead = [];
        $show_tbody = [];

        $i = 1;

        foreach ($fields as $key => $value) {
            $field = $value['name'];
            $label = ucwords(str_replace('_', ' ', $field));
            $headers[] = "<th>{{ _t('{$label}') }}</th>";

            if ($i == 1) {
                $href = "{{ route('{{crud_plural}}.show', \${{crud}}) }}";
                $index_tbody[] = "<td class=\"td-md\"><a href=\"{$href}\">{{ \${{crud}}->{$field} }}</a></td>";
                $first_col = false;
            } else if ($i == count($fields)) {
                $index_tbody[] = "<td>{{ \${{crud}}->{$field} }}</td>";
            } else {
                $index_tbody[] = "<td class=\"td-md\">{{ \${{crud}}->{$field} }}</td>";
            }

            $i++;
            $show_tbody[] = "<td>{{ \${{crud}}->{$field} }}</td>";
        }

        $index_thead_indent = PHP_EOL . '                ';
        $index_thead = implode($index_thead_indent, $headers);

        $index_tbody_indent = PHP_EOL . '                  ';
        $index_tbody = implode($index_tbody_indent, $index_tbody);

        $show_thead_indent = PHP_EOL . '            ';
        $show_thead = implode($show_thead_indent , $headers);

        $show_tbody_indent  = PHP_EOL . '            ';
        $show_tbody = implode($show_tbody_indent, $show_tbody);

        $views_path = base_path('resources/views/');
        $path = $views_path . $crud_plural . '/';

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0755, true);
        }

        $views = ['index', 'create', 'edit', 'fields', 'show'];

        foreach ($views as $view) {
            $stub = File::get(__DIR__ . "/../stubs/{$view}.blade.stub");

            $stub = str_replace('{{index_thead}}', $index_thead, $stub);
            $stub = str_replace('{{index_tbody}}', $index_tbody, $stub);
            $stub = str_replace('{{show_thead}}', $show_thead, $stub);
            $stub = str_replace('{{show_tbody}}', $show_tbody, $stub);
            $stub = str_replace('{{form_fields}}', $form_fields, $stub);

            $stub = str_replace('{{crud}}', $crud, $stub);
            $stub = str_replace('{{crud_cap}}', $crud_cap, $stub);
            $stub = str_replace('{{crud_plural}}', $crud_plural, $stub);
            $stub = str_replace('{{crud_plural_cap}}', $crud_plural_cap, $stub);

            File::put("{$path}{$view}.blade.php", $stub);
        }

        $this->info('Views created successfully.');
    }

    protected function getFieldControl($type)
    {
        if (in_array($type, ['text', 'longText', 'mediumText'])) {
            return 'textarea';
        }

        if (in_array($type, ['timestamp', 'date', 'dateTime'])) {
            return 'date';
        }

        if (in_array($type, ['boolean'])) {
            return 'boolean';
        }

        return 'text';
    }

    protected function getControlStub($type, $field, $label)
    {
        $path = __DIR__ . "/../stubs/fields/{$type}.blade.stub";
        $stub = File::get($path);

        $stub = str_replace('{{field}}', $field, $stub);
        $stub = str_replace('{{label}}', $label, $stub);

        return $stub;
    }
}
