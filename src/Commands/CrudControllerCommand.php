<?php

namespace JoelCuevas\SandboxCrud\Commands;

use Illuminate\Console\GeneratorCommand;

class CrudControllerCommand extends GeneratorCommand
{
    protected $signature = 'make:crud:controller
        {name : The name of the controller.}
        {--resource= : The name of the resource.}';

    protected $description = 'Create the CRUD controller';

    protected $type = 'Controller';

    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());

        $this->replaceNamespace($stub, $name);
        $stub = $this->replaceClass($stub, $name);

        $crud = str_singular(strtolower($this->option('resource')));
        $stub = str_replace('{{crud}}', $crud, $stub);

        $crud_cap = ucfirst($crud);
        $stub = str_replace('{{crud_cap}}', $crud_cap, $stub);

        $crud_plural = str_plural($crud);
        $stub = str_replace('{{crud_plural}}', $crud_plural, $stub);

        $crud_plural_cap = str_plural($crud_cap);
        $stub = str_replace('{{crud_plural_cap}}', $crud_plural_cap, $stub);

        return $stub;
    }

    protected function getStub()
    {
        return __DIR__ . '/../stubs/controller.stub';
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Http\Controllers';
    }
}
