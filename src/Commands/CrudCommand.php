<?php

namespace JoelCuevas\SandboxCrud\Commands;

use Illuminate\Console\Command;

use File;

class CrudCommand extends Command
{
    protected $signature = 'make:crud
        {name : The name of the resource.}
        {--schema= : The schema of the resource.}';

    protected $description = 'Create a CRUD controller, model, views and migration for the resource.';

    public function handle()
    {
        $crud = str_singular(strtolower($this->argument('name')));
        $crud_cap = ucfirst($crud);
        $crud_plural = str_plural($crud);
        $schema = $this->option('schema');
        $migration = "create_{$crud_plural}_table";

        $this->call('make:crud:controller', ['name' => "{$crud_cap}Controller", '--resource' => $crud]);
        $this->call('make:crud:model', ['name' => "Models\\{$crud_cap}", '--resource' => $crud, '--schema' => $schema]);
        $this->call('make:crud:view', ['name' => $crud, '--schema' => $schema]);
        $this->call('make:migration:schema', ['name' => $migration, '--schema' => $schema, '--model' => 0]);

        // add resource to routes
        $routes = app_path('Http/routes.php');
        File::append($routes, "\nRoute::resource('{$crud_plural}', '{$crud_cap}Controller');\n");

        // add softDeletes to migration
        $pattern = glob(base_path("database/migrations/*_{$migration}.php"));
        $file = end($pattern);
        $timestamps = '$table->timestamps();';
        $soft_deletes = "{$timestamps}\n            \$table->softDeletes();";
        $content = str_replace($timestamps, $soft_deletes, File::get($file));
        File::put($file, $content);
    }
}
