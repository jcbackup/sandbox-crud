<?php

namespace JoelCuevas\SandboxCrud\Commands;

use Illuminate\Console\GeneratorCommand;

class CrudModelCommand extends GeneratorCommand
{
    protected $signature = 'make:crud:model
        {name : The name of the model.}
        {--resource= : The name of the resource.}
        {--schema= : The schema of the resource.}';

    protected $description = 'Create the CRUD model';

    protected $type = 'Model';

    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());

        $this->replaceNamespace($stub, $name);
        $stub = $this->replaceClass($stub, $name);

        $crud = str_singular(strtolower($this->option('resource')));

        $table = str_plural($crud);
        $stub = str_replace('{{table}}', $table, $stub);

        $schema = strtolower($this->option('schema'));
        $fields = explode(',', $schema);

        foreach ($fields as $value) {
            $data[] = preg_replace("/(.*?):(.*)/", "$1", trim($value));
        }

        $fillable = "['" . implode("', '", $data) . "']";
        $stub = str_replace('{{fillable}}', $fillable, $stub);

        $rules = [];

        foreach ($data as $field) {
            $rules[] = "'{$field}' => 'required'";
        }

        $rules = "[" . implode(', ', $rules) . "]";
        $stub = str_replace('{{rules}}', $rules, $stub);

        return $stub;
    }

    protected function getStub()
    {
        return __DIR__ . '/../stubs/model.stub';
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace;
    }
}
