<?php

namespace JoelCuevas\SandboxCrud;

use Illuminate\Support\ServiceProvider;

class SandboxCrudServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands(
            'JoelCuevas\SandboxCrud\Commands\CrudCommand',
            'JoelCuevas\SandboxCrud\Commands\CrudControllerCommand',
            'JoelCuevas\SandboxCrud\Commands\CrudModelCommand',
            'JoelCuevas\SandboxCrud\Commands\CrudViewCommand'
        );
    }
}
